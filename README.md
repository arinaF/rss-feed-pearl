# README #

### Project description ###
* Homework for Pearl.
* 	Used technologies and frameworks: Java 8, Spring, Hibernate, Maven, MySQL, JUnit, Mockito, Informa - RSS Library for Java
* 	Java SE Development Kit 8 Version 1.8.0_74 was used to build this project.
* 	Maven 3 (Version 3.3.9) was used to build this project.
	
### Project setup, build and run ###
1. 	Clone the repository using Git.
1. 	Create a schema 'feeds' if not exists using file 'src/main/resources/feeds.sql'.
1. 	Open the project using IDE and pom.xml file.
1. 	Find file 'src/main/resources/app.properties' and change properties 'jdbc.username' and 'jdbc.password'
1. 	There is Tomcat7 Maven plugin with an embedded Apache Tomcat to run the application. 
1. 	In Intellij IDEA choose Run -> Edit Configurations... -> Add New Configuration -> Choose Maven -> In the tab Parameters -> Command line enter - 'tomcat7:run' -> Click Apply and OK.
1. 	Choose Run -> Run 'Unnamed' (Or other configuration name) to run the application.
1. 	After application has started, follow this URL: http://localhost:8080/

### Using application ###
1. 	Click 'Add New Feed' to open form where you can add XML RSS Feed URL un XML RSS Feed Name. If one or both fields are empty or URL could not be parsed - there will be an error message / messages after click 'Add Feed'.
1. 	Click 'List of All XML RSS Feeds' to view the list of all added feeds.
1. 	Choose one of the feeds in the list to open detailed information about the feed.
	
### Time spent for work: ###
* 	29.04.2017 - 7 hours,
* 	01.05.2017 - 5 hours.