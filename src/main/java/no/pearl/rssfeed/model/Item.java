package no.pearl.rssfeed.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ITEMS")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "FEED_ID")
    private int feedId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "LINK")
    private String link;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PUBLISHED")
    private Date published;

    public Item() {
    }

    public Item(int feedId, String title, String link, String description, Date published) {
        this.feedId = feedId;
        this.title = title;
        this.link = link;
        this.description = description;
        this.published = published;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFeedId() {
        return feedId;
    }

    public void setFeedId(int feedId) {
        this.feedId = feedId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (getId() != item.getId()) return false;
        if (getFeedId() != item.getFeedId()) return false;
        if (getTitle() != null ? !getTitle().equals(item.getTitle()) : item.getTitle() != null) return false;
        if (getLink() != null ? !getLink().equals(item.getLink()) : item.getLink() != null) return false;
        if (getDescription() != null ? !getDescription().equals(item.getDescription()) : item.getDescription() != null)
            return false;
        return getPublished() != null ? getPublished().equals(item.getPublished()) : item.getPublished() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getFeedId();
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getLink() != null ? getLink().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getPublished() != null ? getPublished().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", feedId=" + feedId +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", published=" + published +
                '}';
    }
}
