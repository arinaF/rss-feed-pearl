package no.pearl.rssfeed.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "FEEDS")
public class Feed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "URL")
    private String url;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "LAST_UPDATE")
    private Date lastUpdate;

    @Column(name = "FEED_NAME")
    private String feedName;

    public Feed() {
    }

    public Feed(String url, String title, Date lastUpdate, String feedName) {
        this.url = url;
        this.title = title;
        this.lastUpdate = lastUpdate;
        this.feedName = feedName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Feed feed = (Feed) o;

        if (getId() != feed.getId()) return false;
        if (getUrl() != null ? !getUrl().equals(feed.getUrl()) : feed.getUrl() != null) return false;
        if (getTitle() != null ? !getTitle().equals(feed.getTitle()) : feed.getTitle() != null) return false;
        if (getLastUpdate() != null ? !getLastUpdate().equals(feed.getLastUpdate()) : feed.getLastUpdate() != null)
            return false;
        return getFeedName() != null ? getFeedName().equals(feed.getFeedName()) : feed.getFeedName() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getUrl() != null ? getUrl().hashCode() : 0);
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getLastUpdate() != null ? getLastUpdate().hashCode() : 0);
        result = 31 * result + (getFeedName() != null ? getFeedName().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Feed{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", lastUpdate=" + lastUpdate +
                ", feedName='" + feedName + '\'' +
                '}';
    }
}
