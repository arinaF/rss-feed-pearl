package no.pearl.rssfeed.service;

import no.pearl.rssfeed.dto.FeedDetailedDto;
import no.pearl.rssfeed.dto.FeedDto;
import no.pearl.rssfeed.model.Feed;

import java.util.List;

public interface FeedService {

//    Design pattern - 'Facade' - controller uses only FeedService interface as an entry point

    Feed findById(int id);

    void parseAndSaveFeed(FeedDto feedDto);

    List<Feed> findAllFeeds();

    FeedDetailedDto getDetailedDto(int id);
}
