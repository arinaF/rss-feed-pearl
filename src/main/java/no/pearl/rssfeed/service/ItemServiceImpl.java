package no.pearl.rssfeed.service;

import de.nava.informa.core.ItemIF;
import no.pearl.rssfeed.dao.ItemDao;
import no.pearl.rssfeed.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;

@Service("itemService")
@Transactional
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;

    @Override
    public void saveItems(Collection<ItemIF> feedItems, int feedId) {
        if (!CollectionUtils.isEmpty(feedItems)) {
            feedItems.forEach(i -> {
                if (i != null) {
                    Item item = mapItemIFToItem(i, feedId);
                    saveItem(item);
                }
            });
        }
    }

    @Override
    public List<Item> findAllItems(int feedId) {
        return itemDao.findAllItems(feedId);
    }

    @Override
    public void saveItem(Item item) {
        itemDao.saveItem(item);
    }

    public Item mapItemIFToItem(ItemIF ifItem, int feedId) {
        Item item = new Item();
        item.setFeedId(feedId);
        item.setTitle(ifItem.getTitle());
        item.setLink(ifItem.getLink().toString());
        item.setDescription(ifItem.getDescription());
        item.setPublished(ifItem.getDate());
        return item;
    }
}