package no.pearl.rssfeed.service;

import de.nava.informa.core.ChannelIF;
import de.nava.informa.core.ItemIF;
import de.nava.informa.core.ParseException;
import de.nava.informa.impl.basic.ChannelBuilder;
import de.nava.informa.parsers.FeedParser;
import no.pearl.rssfeed.dao.FeedDao;
import no.pearl.rssfeed.dto.FeedDetailedDto;
import no.pearl.rssfeed.dto.FeedDto;
import no.pearl.rssfeed.model.Feed;
import no.pearl.rssfeed.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service("feedService")
@Transactional
public class FeedServiceImpl implements FeedService {

    @Autowired
    private FeedDao feedDao;

    @Autowired
    private ItemService itemService;

    @Override
    public Feed findById(int id) {
        return feedDao.findById(id);
    }

    @Override
    public void parseAndSaveFeed(FeedDto feedDto) {

        try {
            URL feedURL = new URL(feedDto.getUrl());
            ChannelIF parsedChannel = FeedParser.parse(new ChannelBuilder(), feedURL);
            Feed feed = new Feed(feedDto.getUrl(),
                                parsedChannel.getTitle(),
                                parsedChannel.getLastBuildDate(),
                                feedDto.getFeedName());
            feedDao.saveFeed(feed);
            itemService.saveItems(parsedChannel.getItems(), feed.getId());
        } catch (ParseException | IOException e) {
            throw new RuntimeException("Given URL " + "(" + feedDto.getUrl() + ")" +" could not be parsed", e);
        }

    }

    @Override
    public List<Feed> findAllFeeds() {
        return feedDao.findAllFeeds();
    }

    @Override
    public FeedDetailedDto getDetailedDto(int id) {
        FeedDetailedDto dto = new FeedDetailedDto();
        List<Item> feedItems = itemService.findAllItems(id);
        dto.setFeed(findById(id));
        dto.setArticleCount(feedItems.size());
        feedItems.sort(Comparator.comparing(Item::getPublished).reversed());
        dto.setFeedItems(feedItems.stream().limit(5).collect(Collectors.toList()));
        return dto;
    }
}
