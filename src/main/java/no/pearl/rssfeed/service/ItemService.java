package no.pearl.rssfeed.service;

import de.nava.informa.core.ItemIF;
import no.pearl.rssfeed.model.Item;

import java.util.Collection;
import java.util.List;

public interface ItemService {

    void saveItems(Collection<ItemIF> items, int feedId);

    void saveItem(Item item);

    List<Item> findAllItems(int feedId);
}
