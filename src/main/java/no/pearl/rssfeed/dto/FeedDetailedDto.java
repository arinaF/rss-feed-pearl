package no.pearl.rssfeed.dto;

import no.pearl.rssfeed.model.Feed;
import no.pearl.rssfeed.model.Item;

import java.util.Date;
import java.util.List;

public class FeedDetailedDto {
    private Feed feed;
    private Integer articleCount;
    private List<Item> feedItems;

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public Integer getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(Integer articleCount) {
        this.articleCount = articleCount;
    }

    public List<Item> getFeedItems() {
        return feedItems;
    }

    public void setFeedItems(List<Item> feedItems) {
        this.feedItems = feedItems;
    }
}
