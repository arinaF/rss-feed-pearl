package no.pearl.rssfeed.dto;

public class FeedDto {
    private String url;
    private String feedName;

    public FeedDto() {
    }

    public FeedDto(String url, String feedName) {
        this.url = url;
        this.feedName = feedName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }
}
