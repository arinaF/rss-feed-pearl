package no.pearl.rssfeed.controller;

import no.pearl.rssfeed.dto.FeedDetailedDto;
import no.pearl.rssfeed.dto.FeedDto;
import no.pearl.rssfeed.exception.ValidationException;
import no.pearl.rssfeed.model.Feed;
import no.pearl.rssfeed.service.FeedService;
import no.pearl.rssfeed.validator.FeedDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class RssFeedController {

    @Autowired
    private FeedService feedService;

    @Autowired
    private FeedDtoValidator validator;

    @RequestMapping(value = { "/", "/all" }, method = RequestMethod.GET)
    public String getAllFeeds(ModelMap model) {
        List<Feed> feeds = feedService.findAllFeeds();
        model.addAttribute("feeds", feeds);
        return "allfeeds";
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.GET)
    public String newXmlRssFeed(ModelMap model) {
        FeedDto feedDto = new FeedDto();
        model.addAttribute("feed", feedDto);
        return "newfeed";
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.POST)
    public String saveNewXmlRssFeed(FeedDto feedDto, BindingResult result, ModelMap model) {
        List<String> errors = new ArrayList<>();

        validator.validate(feedDto, result);

        if(result.hasErrors()) {
            errors.addAll(result.getAllErrors().stream()
                    .map(ObjectError::getCode)
                    .collect(Collectors.toList()));
            throw new ValidationException(errors);
        }
        try {
            feedService.parseAndSaveFeed(feedDto);
        } catch (RuntimeException e) {
            errors.add(e.getMessage());
            model.addAttribute("errors", errors);
            return "newfeed";
        }

        return "redirect:/all";
    }

    @RequestMapping(value = { "/view/{id}/feed" }, method = RequestMethod.GET)
    public String getDetailedFeed(@PathVariable String id, ModelMap model) {
        FeedDetailedDto detailed = feedService.getDetailedDto(Integer.valueOf(id));
        model.addAttribute("detailedFeed", detailed);
        return "detailedfeed";
    }

    @ExceptionHandler(ValidationException.class)
    public ModelAndView handleValidationException(ValidationException ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("errors", ex.getErrMessages());
        modelAndView.setViewName("newfeed");
        return modelAndView;
    }
}
