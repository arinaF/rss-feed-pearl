package no.pearl.rssfeed.validator;

import no.pearl.rssfeed.dto.FeedDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class FeedDtoValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return FeedDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url", "Feed URL must not be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "feedName", "Feed name must not be empty");
    }
}
