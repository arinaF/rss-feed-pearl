package no.pearl.rssfeed.exception;

import java.util.List;

public class ValidationException extends RuntimeException {
    private List<String> errMessages;

    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(List<String> errMessages) {
        this.errMessages = errMessages;
    }

    public List<String> getErrMessages() {
        return errMessages;
    }

    public void setErrMessages(List<String> errMessages) {
        this.errMessages = errMessages;
    }
}
