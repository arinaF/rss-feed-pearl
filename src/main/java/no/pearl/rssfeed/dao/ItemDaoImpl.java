package no.pearl.rssfeed.dao;

import no.pearl.rssfeed.model.Item;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("itemDao")
public class ItemDaoImpl extends BaseDao<Integer, Item> implements ItemDao{

    @Override
    public void saveItem(Item item) {
        persist(item);
    }

    @Override
    public List<Item> findAllItems(int feedId) {
        Criteria criteria = createEntityCriteria();
        return criteria
                .add(Restrictions.eq("feedId", feedId))
                .list();
    }
}
