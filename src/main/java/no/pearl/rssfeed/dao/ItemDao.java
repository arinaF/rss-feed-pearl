package no.pearl.rssfeed.dao;

import no.pearl.rssfeed.model.Item;

import java.util.List;

public interface ItemDao {

    void saveItem(Item item);

    List<Item> findAllItems(int feedId);
}
