package no.pearl.rssfeed.dao;

import no.pearl.rssfeed.model.Feed;

import java.util.List;

public interface FeedDao {

    Feed findById(int id);

    void saveFeed(Feed feed);

    List<Feed> findAllFeeds();

}
