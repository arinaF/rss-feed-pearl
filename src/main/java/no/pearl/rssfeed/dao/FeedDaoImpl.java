package no.pearl.rssfeed.dao;

import no.pearl.rssfeed.model.Feed;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository("feedDao")
public class FeedDaoImpl extends BaseDao<Integer, Feed> implements FeedDao {

    @Override
    public Feed findById(int id) {
        return getByKey(id);
    }

    @Override
    public void saveFeed(Feed feed) {
        persist(feed);
    }

    @Override
    public List<Feed> findAllFeeds() {
        Criteria criteria = createEntityCriteria();
        return (List<Feed>) criteria.list();
    }
}
