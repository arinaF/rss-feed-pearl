<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>XML RSS Feeds</title>
    <link href="resources/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="border-container">
        <h2>XML RSS Feed</h2>
        <p><em>Please find the list of all available feeds</em></p>
            <c:forEach items="${feeds}" var="feed">
                <p><a href="${pageContext.request.contextPath}/view/${feed.id}/feed" ><c:out value="${feed.feedName}" /></a></p>
            </c:forEach>
        <br/>
        <a href="${pageContext.request.contextPath}/new">Add New Feed</a>
        <br/>
    </div>
</body>
</html>
