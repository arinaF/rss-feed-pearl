<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>New XML RSS Feed</title>
    <link href="resources/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="border-container">
    <h2>Add XML RSS Feed</h2>
    <p><em>Please provide new XML RSS Feed information</em></p>

    <form action="${pageContext.request.contextPath}/new" method="POST" modelAttribute="feed">
        <div class="bg-danger">
        <c:if test="${errors != null}">
            <c:forEach items="${errors}" var="err">
                <p>${err}</p>
            </c:forEach>
        </c:if>
        </div>
        <table class="width-100">
            <tr>
                <td><label for="url">XML RSS Feed URL: </label> </td>
                <td><input class="form-control" name="url" id="url"/></td>
            </tr>
            <tr>
                <td><label for="feedName">XML RSS Feed Name: </label> </td>
                <td><input class="form-control" name="feedName" id="feedName"/></td>
            </tr>

            <tr>
                <td colspan="3">
                    <input class="btn" type="submit" value="Add Feed"/>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    <a href="${pageContext.request.contextPath}/all">List of All XML RSS Feeds</a>
</div>
</body>
</html>
