<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Detailed XML RSS Feed</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid">
        <br/>
        <a href="${pageContext.request.contextPath}/all">List of All XML RSS Feeds</a>
        <h2>XML RSS Feed</h2>
        <p><em>Please find the detailed information about feed</em></p>
        <p><label>Feed Name: </label> ${detailedFeed.feed.feedName}</p>
        <p><label>Feed URL: </label> <a href="${detailedFeed.feed.url}" target="_blank">${detailedFeed.feed.url}</a></p>
        <p><label>Last Update: </label> ${detailedFeed.feed.lastUpdate}</p>
        <p><label>Article Count: </label> ${detailedFeed.articleCount}</p>
        <label>Most recent articles: </label>
        <div class="container-fluid">
            <c:forEach items="${detailedFeed.feedItems}" var="item" varStatus="index">
                <p>${index.index+1}</p>
                <p><c:out value="${item.title}" /></p>
                <p><a href="${item.link}" target="_blank"><c:out value="${item.link}" /></a></p>
                <hr>
            </c:forEach>
        </div>
        <a href="${pageContext.request.contextPath}/all">List of All XML RSS Feeds</a>
        <br/>
    </div>
</body>
</html>