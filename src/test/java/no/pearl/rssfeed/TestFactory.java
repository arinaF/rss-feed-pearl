package no.pearl.rssfeed;

import de.nava.informa.core.ItemIF;
import no.pearl.rssfeed.model.Item;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TestFactory {

    public static List<Item> getItemList() throws ParseException {
        List<Item> items = new ArrayList<>();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        items.add(new Item(1, "title1", "link", "descr", format.parse("2016-05-06")));
        items.add(new Item(2, "title2", "link", "descr", format.parse("2016-05-03")));
        items.add(new Item(3, "title3", "link", "descr", format.parse("2016-05-04")));
        items.add(new Item(4, "title4", "link", "descr", format.parse("2016-05-01")));
        items.add(new Item(5, "title5", "link", "descr", format.parse("2016-05-05")));
        items.add(new Item(6, "title6", "link", "descr", format.parse("2016-05-02")));

        return items;
    }

    public static List<ItemIF> getItemIFList() throws MalformedURLException {
        List<ItemIF> items = new ArrayList<>();

        ItemIF item1 = new de.nava.informa.impl.basic.Item("test1", "test1", new URL("https://test1.lv/test1"));
        ItemIF item2 = new de.nava.informa.impl.basic.Item("test2", "test2", new URL("https://test2.lv/test2"));
        items.add(item1);
        items.add(item2);
        return items;
    }
}
