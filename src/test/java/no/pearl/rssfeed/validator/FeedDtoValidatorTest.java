package no.pearl.rssfeed.validator;

import no.pearl.rssfeed.config.ApplicationConfig;
import no.pearl.rssfeed.dto.FeedDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationConfig.class)
public class FeedDtoValidatorTest {

    private final FeedDtoValidator feedDtoValidator = new FeedDtoValidator();

    private FeedDto feedDto;

    private BindingResult result;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        feedDto = new FeedDto();
        feedDto.setUrl("");
        feedDto.setFeedName("");
        result = new BeanPropertyBindingResult(feedDto, "feedDto");
    }

    @Test
    public void support() {
        assertTrue(feedDtoValidator.supports(FeedDto.class));
    }

    @Test
    public void validate() {
        feedDtoValidator.validate(feedDto, result);
        assertEquals(2, result.getErrorCount());
        List<String> errorCodes = result.getAllErrors()
                .stream()
                .map(ObjectError::getCode)
                .collect(Collectors.toList());
        assertTrue(errorCodes.contains("Feed URL must not be empty"));
        assertTrue(errorCodes.contains("Feed name must not be empty"));
    }

}
