package no.pearl.rssfeed.service;

import no.pearl.rssfeed.TestFactory;
import no.pearl.rssfeed.config.ApplicationConfig;
import no.pearl.rssfeed.dao.FeedDao;
import no.pearl.rssfeed.dto.FeedDetailedDto;
import no.pearl.rssfeed.dto.FeedDto;
import no.pearl.rssfeed.model.Feed;
import no.pearl.rssfeed.model.Item;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationConfig.class)
public class FeedServiceTest {

    @Mock
    private FeedDao feedDao;

    @Mock
    private ItemService itemService;

    @InjectMocks
    private FeedServiceImpl feedServiceImpl;

    private Feed feed;
    private FeedDto feedDto;
    private List<Item> items;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        feed = new Feed("http://www.15min.lt/rss", "title", new Date(), "feedName");
        feedDto = new FeedDto("http://www.15min.lt/rss", "feedName");
        try {
            items = TestFactory.getItemList();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        when(feedDao.findById(anyInt())).thenReturn(feed);
        when(feedDao.findAllFeeds()).thenReturn(new ArrayList<Feed>());
        when(itemService.findAllItems(anyInt())).thenReturn(items);

        doNothing().when(itemService).saveItems(anyCollection(), anyInt());
        doNothing().when(feedDao).saveFeed(any(Feed.class));
    }

    @Test
    public void findById() {
        Feed feedFromDb = feedServiceImpl.findById(anyInt());
        verify(feedDao, times(1)).findById(anyInt());
        assertEquals(feedFromDb, feed);
    }

    @Ignore
    @Test
    public void parseAndSaveFeed() {
        feedServiceImpl.parseAndSaveFeed(feedDto);
        verify(feedDao, times(1)).saveFeed(any(Feed.class));
        verify(itemService, times(1)).saveItems(anyCollection(), anyInt());
    }

    @Test(expected = RuntimeException.class)
    public void parseAndSaveFeed_RuntimeException() {
        feedDto.setUrl("wrongURL");
        feedServiceImpl.parseAndSaveFeed(feedDto);

        verify(feedDao, never()).saveFeed(any(Feed.class));
        verify(itemService, never()).saveItems(anyCollection(), anyInt());
    }

    @Test
    public void findAllFeeds() {
        feedServiceImpl.findAllFeeds();
        verify(feedDao, times(1)).findAllFeeds();
    }

    @Test
    public void getDetailedFeedDto() {
        FeedDetailedDto detailedDto = feedServiceImpl.getDetailedDto(1);
        assertTrue(detailedDto.getFeed() != null);
        assertEquals(detailedDto.getFeed().getFeedName(), "feedName");
        assertEquals(detailedDto.getArticleCount(), Integer.valueOf(6));
        assertTrue(detailedDto.getFeedItems() != null);
        assertEquals(detailedDto.getFeedItems().size(), 5);
        assertFalse(detailedDto.getFeedItems().stream().map(Item::getId).collect(Collectors.toList()).contains(6));
    }

}
