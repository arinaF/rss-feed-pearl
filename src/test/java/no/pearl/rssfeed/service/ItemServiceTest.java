package no.pearl.rssfeed.service;

import de.nava.informa.core.ItemIF;
import no.pearl.rssfeed.TestFactory;
import no.pearl.rssfeed.config.ApplicationConfig;
import no.pearl.rssfeed.dao.ItemDao;
import no.pearl.rssfeed.model.Item;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = ApplicationConfig.class)
public class ItemServiceTest {

    @Mock
    private ItemDao itemDao;

    @InjectMocks
    ItemServiceImpl itemServiceImpl;

    private List<ItemIF> items;

    @Before
    public void setUp() throws MalformedURLException {
        MockitoAnnotations.initMocks(this);
        items = TestFactory.getItemIFList();
        doNothing().when(itemDao).saveItem(any(Item.class));
    }

    @Test
    public void saveItems_fullItemsList() {
        itemServiceImpl.saveItems(items, 300);
        verify(itemDao, times(2)).saveItem(any(Item.class));
    }

    @Test
    public void saveItems_emptyItemsList() {
        itemServiceImpl.saveItems(new ArrayList<>(), 300);
        verify(itemDao, never()).saveItem(any(Item.class));
    }

    @Test
    public void findAllItems() {
        itemServiceImpl.findAllItems(anyInt());
        verify(itemDao, times(1)).findAllItems(anyInt());
    }

    @Test
    public void saveItem() {
        itemServiceImpl.saveItem(any(Item.class));
        verify(itemDao, times(1)).saveItem(any(Item.class));
    }

    @Test
    public void mapItemIFToItem() {
        ItemIF itemIf = items.get(0);
        Item item = itemServiceImpl.mapItemIFToItem(itemIf, 300);
        assertEquals(item.getFeedId(), 300);
        assertEquals(itemIf.getTitle(), item.getTitle());
        assertEquals(itemIf.getLink().toString(), item.getLink());
        assertEquals(itemIf.getDescription(), item.getDescription());
        assertEquals(itemIf.getDate(), item.getPublished());
    }
}